/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <stdexcept>
#include <string>


namespace textgen {

// TODO(bgaifullin) add filename and line number to error

class error : public std::logic_error
{
public:
  using std::logic_error::logic_error;
};

class invalid_data : public error
{
public:
  using error::error;
};

class reader_error : public error
{
public:
  using error::error;
};

class not_found_error : public error
{
public:
  using error::error;
};

class io_error : public error
{
public:
  using error::error;
};

} // textgen
