/**
Created by Bulat Gaifullin on 15/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <logger.hpp>

#include <boost/noncopyable.hpp>

#include <vector>

namespace textgen {

typedef std::function<void (const char* s, std::size_t n)> data_consumer;

/**
 * data reader interface
 */
class data_reader : boost::noncopyable
{
public:
  virtual ~data_reader() {}
  /**
   * read the whole content of file which is located by %url%
   */
  virtual void process(const std::vector<std::string>& urls, data_consumer consumer) = 0;
};

namespace details {

/**
 * data reader based on utility 'curl'
 */
class curl_data_reader : public data_reader
{
public:
  /**
   * constructor
   * @param retry_num: number of retries
   * @param retry_delay: the delay in sec between retries. 0 means progressive retries.
   *                      additional information can be found by `man curl`
   */
  curl_data_reader(logger::ptr log, std::uint16_t retry_num=0, std::uint16_t retry_delay=0)
    : logger_(log)
    , retry_num_(retry_num)
    , retry_delay_(retry_delay)
  {
  }

  virtual void process(const std::vector<std::string>& urls, data_consumer consumer);

private:
  void download(std::vector<std::string>& args, data_consumer& consumer, std::vector<char>& read_buffer);
private:
  logger::ptr logger_;
  std::uint16_t retry_num_;
  std::uint16_t retry_delay_;
};

} // details;

/// symlink
typedef details::curl_data_reader default_data_reader;

} // textgen
