/**
Created by Bulat Gaifullin on 18/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <string_ref.hpp>

#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>


namespace textgen {

class symbol;

/**
 * class to store all symbols which are found in the model
 */
class symbols_table : public std::enable_shared_from_this<symbols_table>
{
public:
  typedef std::shared_ptr<symbols_table> ptr;
  typedef std::shared_ptr<const symbols_table> const_ptr;

  symbols_table()
  {
  }

  /**
   * add new symbol to table, if symbol already exists, nothin will happen
   * @param s string representation of symbol
   * @return added symbol or existing one
   */
  symbol add(const std::string& s);

  /**
   * get symbol by string representation
   * @param s string representation of symbol
   * @return symbol if it is found, otherwise throw not_found_error
   */
  symbol get(const std::string& s) const;

  /**
   * get total size of storage
   */
  std::size_t size() const
  {
    return storage_.size();
  }

private:
  const char* get(std::size_t offset) const
  {
    return &storage_[offset];
  }

  friend class symbol;

  friend void dump(std::ostream&, const symbols_table&);
  friend void load(std::istream&, symbols_table&);

private:
  // TODO(bgaifullin) may be realization with list of vectors with fixed size will be more effective
  std::vector<char> storage_;
  std::unordered_map<details::string_ref, std::size_t> map_;
};

/**
 * the unit of model
 */
class symbol
{
public:
  symbol(symbols_table::const_ptr t, std::size_t offset, std::size_t size)
    : table_(t), offset_(offset), size_(size)
  {
  }

  /**
   * get string representation of symbol
   */
  std::string str() const
  {
    return std::string(table_->get(offset_), size_);
  }

  /**
   * get offset
   */
  std::size_t offset() const
  {
    return offset_;
  }

  /**
   * get size
   */
  std::size_t size() const
  {
    return size_;
  }

  /**
   * get raw data
   */
  const char* data() const
  {
    return table_->get(offset_);
  }

  friend void dump(std::ostream&, const symbol&);
  friend void load(std::istream&, symbol&);
private:
  symbols_table::const_ptr table_;
  std::size_t offset_;
  std::size_t size_;
};

} // textgen


namespace boost {

// boost::hash specification,  used by boost::hash_range

template <>
struct hash<textgen::symbol> : std::unary_function<std::size_t, textgen::symbol>
{
  std::size_t operator()(const textgen::symbol& seq) const
  {
    std::size_t seed = hash_value(seq.data());
    hash_combine(seed, seq.size());
    return seed;
  }
};

} // boost

namespace std {
  // the hash functions in std namespace
  template<>
  struct hash<textgen::symbol> : boost::hash<textgen::symbol>
  {
  };

  template<>
  struct equal_to<textgen::symbol> : binary_function<bool, textgen::symbol, textgen::symbol>
  {
    bool operator()(textgen::symbol const& lhs, textgen::symbol const& rhs) const
    {
      return lhs.size() == rhs.size() && lhs.data() == rhs.data();
    }
  };
} // std
