/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <model.hpp>

#include <iostream>

namespace textgen {

/// TODO(bgaifullin) it will be better to use operators <<, >> with custom stream that implements binary protocol
/// symbols table
void dump(std::ostream& s, const symbols_table& o);
void load(std::istream& s, symbols_table& o);


/// symbol
void dump(std::ostream& s, const symbol& o);
void load(std::istream& s, symbol& o);

/// sequence
void dump(std::ostream& s, const sequence& o);
void load(std::istream& s, sequence& o);

/// model
void dump(std::ostream& s, const model& o);
void load(std::istream& s, model& o);

}