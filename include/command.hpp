/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <logger.hpp>

#include <boost/program_options.hpp>

#include <memory>

namespace textgen {

using boost::program_options::options_description;

using boost::program_options::variables_map;

/**
 * base class for execution unit
 */
class command
{
public:
  typedef std::shared_ptr<command> ptr;

public:
  /// returns unique name of command
  virtual const char* name() const = 0;

  /// returns short description of command, will be used in help message
  virtual const char* description() const = 0;

  /// added custom command line options which are specific for command
  virtual options_description get_options() const = 0;

  /// entry point for command
  virtual void take_action(const variables_map& vars, logger::ptr log) = 0;
};

} // textgen