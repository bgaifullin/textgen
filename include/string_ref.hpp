/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <boost/functional/hash.hpp>

#include <functional>
#include <string>
#include <vector>


namespace textgen {
namespace details {
/// helper class to which does not own buffer and should be used very carefully only for internal purpose
struct string_ref
{
  explicit string_ref(const std::string& s)
    : ptr_(s.data())
    , offset_(std::string::npos)
    , size_(s.size())
    , hash_(boost::hash_range(ptr_, ptr_ + size_))
  {
  }

  string_ref(std::vector<char>& v, std::size_t offset, std::size_t size)
    : buffer_(&v)
    , offset_(offset)
    , size_(size)
    , hash_(boost::hash_range(buffer_->data() + offset_, buffer_->data() + offset_ + size_))
  {
  }

  const char* ptr() const
  {
    if (offset_ == std::string::npos)
      return ptr_;
    return buffer_->data() + offset_;
  }

  std::size_t size() const
  {
    return size_;
  }

  std::size_t hash() const
  {
    return hash_;
  }

private:
  union {
    const char* ptr_;
    const std::vector<char>* buffer_;
  };

  std::size_t size_;
  std::size_t offset_;
  std::size_t hash_;
};
} // details
} // textgen

namespace std {
template <>
struct hash<textgen::details::string_ref> : std::unary_function<std::size_t, textgen::details::string_ref>
{
  std::size_t operator()(const textgen::details::string_ref& v) const
  {
    return v.hash();
  }
};


template <>
struct equal_to<textgen::details::string_ref> :
  std::binary_function<bool, textgen::details::string_ref, textgen::details::string_ref>
{
  bool operator()(const textgen::details::string_ref& v1, const textgen::details::string_ref& v2) const
  {
    if (v1.size() == v2.size())
    {
      return !std::char_traits<char>::compare(v1.ptr(), v2.ptr(), v1.size());
    }
    return false;
  }
};

} // std
