/**
Created by Bulat Gaifullin on 17/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <symbols.hpp>
#include <sequence.hpp>

#include <boost/noncopyable.hpp>

#include <map>
#include <unordered_map>
#include <vector>


namespace textgen {

/**
 * the model of data
 * represents mark chain with rank N
 */
class model : boost::noncopyable
{
public:
  typedef std::shared_ptr<model> ptr;
  typedef std::shared_ptr<const model> const_ptr;

  enum
  {
    max_weight = 1000
  };

  model(std::size_t rank, symbols_table::ptr table)
    : rank_(rank)
    , symbols_(table)
  {
  }

  /**
   * add new item to model
   * @param seq the sequence of symbols with length N
   * @param sym the symbol followed by sequence
   * @param weight the aligned weight of the symbol.
   *         weight can be calculated by following formula:
   *         weight[i + 1] = sum(probabilities[0..i+1])
   */
  void add(const sequence& seq, const symbol& sym, std::uint32_t weight);

  /**
   * get next symbol followed by sequence
   * @param seq the sequence of symbols which size N, if size less than N invalid_data error will be threw
   * @param weight, element with weight greater or equal specified will be selected
   * @return symbol follwoded by specified sequence
   */
  symbol next(const sequence& seq, std::uint32_t weight) const;

  /**
   * get rank of model
   */
  std::uint8_t rank() const
  {
    return rank_;
  }

  /**
   * find symbol by string
   * @param s string represenation of symbol
   * @return symbol if it is found otherwise raise not_found_error
   */
  symbol resolve(const std::string& s) const
  {
    return symbols_->get(s);
  }

  sequence resolve_all(const std::vector<std::string> v) const
  {
    sequence::container_type tmp;
    for (auto& i : v)
    {
      tmp.push_back(resolve(i));
    }
    return sequence(std::move(tmp));
  }

  /**
   * continiously calculate weight for elemenent, weight of next element depends on weight previous one
   * Note: frequency of elements should be in ascending order
   */
  static std::uint32_t calculate_weight(std::uint32_t& seed, std::uint32_t frequency, std::uint32_t count)
  {
    seed += frequency;
    return static_cast<std::uint32_t>((static_cast<std::uint64_t>(max_weight) * seed) / count);
  }

  friend void dump(std::ostream&, const model&);
  friend void load(std::istream&, model&);
private:
  std::size_t rank_;
  symbols_table::ptr symbols_;
  /// sequence -> weight -> next symbol
  std::unordered_map<sequence, std::map<std::uint32_t, symbol>> mark_chain_;
};

} // textgen
