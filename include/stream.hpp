/**
Created by Bulat Gaifullin on 15/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <boost/noncopyable.hpp>

#include <functional>
#include <memory>
#include <iostream>

namespace textgen {

typedef std::unique_ptr<std::istream, std::function<void (std::istream*)>> istream_holder;

typedef std::unique_ptr<std::ostream, std::function<void (std::ostream*)>> ostream_holder;

/*
 * open file for reading
 */
istream_holder open_for_read(const std::string& filepath);

/*
 * open file for writing
 */
ostream_holder open_for_write(const std::string& filepath);

/*
 * get holder to standard input stream
 */
istream_holder open_stdin();

/*
 * get holder to standard output stream
 */
ostream_holder open_stdout();

/*
 * get holder to standard error stream
 */
ostream_holder open_stderr();

} // textgen
