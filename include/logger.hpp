/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <stream.hpp>

#include <boost/format.hpp>
#include <boost/noncopyable.hpp>


namespace textgen {

namespace detail {

inline std::string format(boost::format&& fmt)
{
  return fmt.str();
}

/**
 * helper function for recursive format string
 */
template <typename Head, typename ...Tail>
std::string format(boost::format&& fmt, const Head& head, const Tail&...tail)
{
  return format(fmt % head, tail...);
}

template <typename Head>
std::string format(boost::format&& fmt, const Head& head)
{
  return format(std::forward<boost::format>(fmt % head));
}

} //detail

/**
 * logger class
 */
class logger : boost::noncopyable
{
public:
  enum level
  {
    DEBUG = 0,
    INFO,
    WARNING,
    ERROR
  };

  typedef std::shared_ptr<logger> ptr;

public:
  logger(level l, ostream_holder&& stream)
    : level_(l)
    , stream_(std::forward<ostream_holder>(stream))
  {
  }

  level get_level() const
  {
    return level_;
  }

  template <typename ...Args>
  void debug(const std::string& message, const Args&... args)
  {
    if (level_ <= DEBUG)
    {
      write(DEBUG, detail::format(boost::format(message), args...));
    }
  }

  template <typename ...Args>
  void info(const std::string& message, const Args&... args)
  {
    if (level_ <= INFO)
    {
      write(INFO, detail::format(boost::format(message), args...));
    }
  }

  template <typename ...Args>
  void warning(const std::string& message, const Args&... args)
  {
    if (level_ <= WARNING)
    {
      write(WARNING, detail::format(boost::format(message), args...));
    }
  }

  template <typename ...Args>
  void error(const std::string& message, const Args&... args)
  {
    if (level_ <= ERROR)
    {
      write(ERROR, detail::format(boost::format(message), args...));
    }
  }

private:
  void write(level l, const std::string& message);

private:
  level level_;
  ostream_holder stream_;
};

}
