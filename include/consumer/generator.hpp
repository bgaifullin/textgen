/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <model.hpp>

#include <boost/noncopyable.hpp>

#include <random>

namespace textgen {
namespace consumer {

class text_generator : boost::noncopyable
{
public:
  text_generator(model::const_ptr m, const std::vector<std::string>& initial_sec, std::size_t l);

  /**
   * consume next token from model
   */
  std::string next();

  /**
   * return True if end is reached
   */
  bool is_valid() const;

private:
  std::string get_next();
private:
  model::const_ptr model_;
  sequence sequence_;
  std::size_t length_;
  std::default_random_engine random_engine_;
};

} // consumer
} // textgen
