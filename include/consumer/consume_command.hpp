/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <consumer/generator.hpp>
#include <command.hpp>

#include <memory>

namespace textgen {
namespace consumer {
/**
 * generator entry point
 */
class consume_command : public command
{
public:
  virtual const char* name() const { return "consume"; }

  virtual const char* description() const { return "consumes text from model"; };

  virtual options_description get_options() const;

  virtual void take_action(const variables_map& vars, logger::ptr log);

protected:
  virtual model::const_ptr load_model(const std::string& filepath) const;

  virtual std::unique_ptr<text_generator> make_generator(
    model::const_ptr m, const std::vector<std::string>& v, std::size_t l) const;
};

} // trainer
} // textgen
