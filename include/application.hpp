/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <command.hpp>

#include <boost/noncopyable.hpp>

namespace textgen {

/**
 * application class, the entry point of program
 */
class application : boost::noncopyable
{
public:
  application(std::initializer_list<command::ptr> commands)
    : commands_(commands)
  {
  }

  /**
   * select command according to specified arguments and executes it
   * @return command exit code
   */
  int run(int argc, const char** argv) noexcept;

private:
  /**
   * parse arguments and transfer control to command executor
   */
  void start(int argc, const char** argv);

  /**
   * run selected command
   */
  void run_command(command::ptr cmd, const variables_map& vars);

  /**
   * application level options
   */
  options_description get_common_options() const;

  /**
   * get available commands descriptions
   */
  options_description get_available_commands() const;

  /**
   * find picked command
   */
  command::ptr find_command(const variables_map& parsed) const;

private:
  std::vector<command::ptr> commands_;
};

} // textgen