/**
Created by Bulat Gaifullin on 16/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <boost/locale/utf.hpp>

#include <functional>
#include <locale>
#include <deque>
#include <vector>

namespace textgen {

typedef std::function< void (std::string&&)> token_consumer;

using boost::locale::utf::code_point;

typedef std::function<bool (code_point, const std::locale&)> separator;

/**
 * class to split text into tokens
 */
class tokenizer
{
public:
  tokenizer(token_consumer&& consumer, separator&& sep);

  /**
   * consume chunk and produce tokens
   */
  void operator()(const char* s, std::size_t n);

private:
  void on_next_chunk(const char* s, std::size_t n);

  void on_next_token();

private:
  token_consumer consumer_;
  separator separator_;
  code_point tail_buffer_;
  std::size_t tail_size_;
  std::vector<char> buffer_;
  std::locale locale_;
};

} // textgen
