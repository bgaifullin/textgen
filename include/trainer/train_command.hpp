/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <data_reader.hpp>
#include <command.hpp>

namespace textgen {
namespace trainer {
/**
 * trainer entry point
 */
class train_command : public command
{
public:
  virtual const char* name() const { return "train"; }

  virtual const char* description() const { return "trains new model"; };

  virtual options_description get_options() const;

  virtual void take_action(const variables_map& vars, logger::ptr log);

protected:
  virtual std::unique_ptr<data_reader> make_data_reader(const variables_map& vars, logger::ptr log) const;
};

} // trainer
} // textgen
