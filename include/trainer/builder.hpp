/**
Created by Bulat Gaifullin on 18/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <model.hpp>

#include <boost/noncopyable.hpp>

#include <vector>

namespace textgen {
namespace trainer {

/**
 * model builder class
 */
class builder : boost::noncopyable
{
public:
  builder(std::size_t model_rank);

  /**
   * add next item to chain
   */
  void add(std::string&& s);

  /**
   * get model which was built at current moment
   */
  model::ptr get_result() const;

private:
  void init_sequence(symbol&& s);

  void add_to_model(symbol&& s);

private:
  typedef  void (builder::*consumer_ptr)(symbol&& s);

  std::size_t model_rank_;
  sequence::container_type initial_vec_;
  symbols_table::ptr symbols_;
  std::unique_ptr<sequence> current_sequence_;
  // chain, next symbol, frequency
  std::unordered_map<sequence, std::unordered_map<symbol, std::uint32_t>> cache_;
  consumer_ptr consumer_;
};

} // trainer
} // textgen
