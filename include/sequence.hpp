/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#pragma once

#include <symbols.hpp>

#include <algorithm>
#include <functional>
#include <list>


namespace textgen {
/**
 *  the sequence of symbols with fixed size.
 */
class sequence
{
public:
  typedef std::list<symbol> container_type;
  typedef container_type::const_iterator iterator_type;

  sequence(container_type&& items)
    : hash_func_(std::hash<container_type::value_type>())
    , items_(std::forward<container_type>(items))
    , hash_(0)
  {
    for (auto& i : items_)
    {
      on_push(i);
    }
  }

  sequence(std::initializer_list<container_type::value_type>&& items) : sequence(container_type(items))
  {
  }

  /**
   * go to right, pop item from left
   * @return removed symbol
   */
  symbol shift()
  {
    auto tmp = std::move(items_.front());
    items_.pop_front();
    on_pop(tmp);
    return tmp;
  }

  /**
  * shift and eat new symbol
  */
  symbol shift_and_eat(symbol&& s)
  {
    auto tmp(shift());
    eat(std::forward<symbol>(s));
    return tmp;
  }

  /**
   * add new symbol to right end of sequence
   */
  sequence& eat(symbol&& s)
  {
    items_.emplace_back(std::forward<symbol>(s));
    on_push(items_.back());
    return *this;
  }

  iterator_type begin() const
  {
    return items_.begin();
  }

  iterator_type end() const
  {
    return items_.end();
  }

  std::size_t size() const
  {
    return items_.size();
  }

  std::size_t hash() const
  {
    return hash_;
  }

private:
  void on_pop(const symbol& s)
  {
    hash_ ^= hash_func_(s);
  }

  void on_push(const symbol& s)
  {
    hash_ ^= hash_func_(s);
  }

  friend void dump(std::ostream&, const sequence&);
  friend void load(std::istream&, sequence&);
private:
  std::hash<container_type::value_type> hash_func_;
  container_type items_;
  std::size_t hash_;
};

} // textgen


namespace std {

template <>
struct hash<textgen::sequence> : std::unary_function<std::size_t, textgen::sequence>
{
  std::size_t operator()(const textgen::sequence& seq) const
  {
    return seq.hash();
  }
};

template<>
struct equal_to<textgen::sequence> : binary_function<bool, textgen::sequence, textgen::sequence>
{
  bool operator()(textgen::sequence const& lhs, textgen::sequence const& rhs) const
  {
    return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), equal_to<textgen::symbol>());
  }
};

} // std
