/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <stream.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class streams_test : public testing::Test
{
};

TEST_F(streams_test, test_get_stdout)
{
  auto stdout = open_stdout();
  testing::internal::CaptureStdout();
  (*stdout) << "test";
  ASSERT_EQ("test", testing::internal::GetCapturedStdout());
}

TEST_F(streams_test, test_get_stderr)
{
  auto stderr = open_stderr();
  testing::internal::CaptureStderr();
  (*stderr) << "test";
  ASSERT_EQ("test", testing::internal::GetCapturedStderr());
}

TEST_F(streams_test, test_get_stdin)
{
  auto stdin = open_stdin();
  ASSERT_EQ(reinterpret_cast<void*>(&std::cin), reinterpret_cast<void*>(stdin.get()));
}
