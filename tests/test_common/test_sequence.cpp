/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <sequence.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class sequence_test : public testing::Test
{
public:
  sequence_test();

protected:
  symbols_table::ptr symbols_;
};


sequence_test::sequence_test()
 : symbols_(std::make_shared<symbols_table>())
{
  symbols_->add("unit");
  symbols_->add("test");
}


TEST_F(sequence_test, test_cyclic_hash_are_used)
{
  sequence seq(std::move(sequence::container_type()));
  ASSERT_EQ(0, seq.hash());
  seq.eat(symbols_->get("test"));
  auto h1 = seq.hash();
  ASSERT_NE(0, h1);
  seq.eat(symbols_->get("unit"));
  ASSERT_NE(h1, seq.hash());
  seq.shift_and_eat(symbols_->get("test"));
  seq.shift();
  ASSERT_EQ(h1, seq.hash());
  seq.shift();
  ASSERT_EQ(0, seq.hash());
}


TEST_F(sequence_test, test_shift_and_eat_keeps_size)
{
  sequence seq(std::move(sequence::container_type()));
  ASSERT_EQ(0, seq.size());
  seq.eat(symbols_->get("unit"));
  ASSERT_EQ(1, seq.size());
  seq.shift_and_eat(symbols_->get("test"));
  ASSERT_EQ(1, seq.size());
  seq.shift();
  ASSERT_EQ(0, seq.size());
}
