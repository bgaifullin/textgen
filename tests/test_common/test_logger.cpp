/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <logger.hpp>

#include <gtest/gtest.h>

using namespace textgen;

class logger_test : public testing::Test
{
};


TEST_F(logger_test, test_method_for_selected_level)
{
  testing::internal::CaptureStderr();
  logger log(logger::DEBUG, open_stderr());
  log.debug("test %d", 1);
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "test 1", testing::internal::GetCapturedStderr());
}

TEST_F(logger_test, test_method_for_lower_level)
{
  testing::internal::CaptureStderr();
  logger log(logger::DEBUG, open_stderr());
  log.info("test %d", 1);
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "test 1", testing::internal::GetCapturedStderr());
}

TEST_F(logger_test, test_method_for_higher_level)
{
  testing::internal::CaptureStderr();
  logger log(logger::ERROR, open_stderr());
  log.warning("test %d", 1);
  ASSERT_EQ("", testing::internal::GetCapturedStderr());
}
