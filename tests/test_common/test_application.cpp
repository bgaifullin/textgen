/**
Created by Bulat Gaifullin on 14/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <application.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace textgen;

using testing::_;


class mock_command: public command
{
public:
  MOCK_CONST_METHOD0(name, const char* ());
  MOCK_CONST_METHOD0(description, const char* ());
  MOCK_CONST_METHOD0(get_options, options_description());
  MOCK_METHOD2(take_action, void (const variables_map&, logger::ptr));
};

class application_test : public testing::Test
{
protected:
  std::shared_ptr<mock_command> create_command(const char* name, const char* desc);
};


std::shared_ptr<mock_command> application_test::create_command(const char* name, const char* desc)
{
  options_description args("command options");
  args.add_options()("option", "option one");
  std::shared_ptr<mock_command> cmd_ptr(std::make_shared<mock_command>());
  mock_command& cmd = *cmd_ptr;

  ON_CALL(cmd, name()).WillByDefault(testing::Return(name));
  EXPECT_CALL(cmd, name()).Times(testing::AtLeast(1));
  ON_CALL(cmd, description()).WillByDefault(testing::Return(desc));
  EXPECT_CALL(cmd, description());
  ON_CALL(cmd, get_options()).WillByDefault(testing::Return(args));

  return cmd_ptr;
}

TEST_F(application_test, test_show_help)
{
  const char* argv[] = {"test", "--help"};
  const int argc = sizeof(argv) / sizeof(argv[0]);

  auto cmd = create_command("test", "test command");
  EXPECT_CALL(*cmd, get_options()).Times(0);

  application app({cmd});
  testing::internal::CaptureStdout();
  ASSERT_EQ(0, app.run(argc, argv));

  const std::string& stdout = testing::internal::GetCapturedStdout();
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "test command", stdout);
  ASSERT_PRED_FORMAT2(::testing::IsNotSubstring, "option one", stdout);
}


TEST_F(application_test, test_show_command_help)
{
  const char* argv[] = {"test", "--test", "--help"};
  const int argc = sizeof(argv) / sizeof(argv[0]);

  auto cmd = create_command("test", "test command");
  EXPECT_CALL(*cmd, get_options());

  application app({cmd});
  testing::internal::CaptureStdout();
  ASSERT_EQ(0, app.run(argc, argv));

  const std::string& stdout = testing::internal::GetCapturedStdout();
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "command option", stdout);
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "option one", stdout);
  ASSERT_PRED_FORMAT2(::testing::IsNotSubstring, "common options", stdout);
}


TEST_F(application_test, test_run_fails_if_several_commands_selected)
{
  const char* argv[] = {"test", "--cmd_one", "--cmd_two"};
  const int argc = sizeof(argv) / sizeof(argv[0]);

  application app({create_command("cmd_one", "command one"), create_command("cmd_two", "command two")});
  testing::internal::CaptureStderr();
  ASSERT_EQ(1, app.run(argc, argv));

  const std::string& stderr = testing::internal::GetCapturedStderr();
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "option '{command}' cannot be specified more than once", stderr);
}

TEST_F(application_test, test_run_fails_if_no_command)
{
  const char* argv[] = {"test"};
  const int argc = sizeof(argv) / sizeof(argv[0]);

  application app({create_command("test", "test command")});
  testing::internal::CaptureStderr();
  ASSERT_EQ(1, app.run(argc, argv));
  const std::string& stderr = testing::internal::GetCapturedStderr();
  ASSERT_PRED_FORMAT2(::testing::IsSubstring, "option '{command}' is required", stderr);
}


TEST_F(application_test, test_invoke_command)
{
  const char* argv[] = {"test", "--test", "--option"};
  const int argc = sizeof(argv) / sizeof(argv[0]);

  auto has_option = [](const variables_map& vars) -> bool { return vars.count("option") > 0; };
  auto cmd = create_command("test", "test command");

  EXPECT_CALL(*cmd, get_options());
  EXPECT_CALL(*cmd, take_action(testing::Truly(has_option), _));
  application app({cmd});
  ASSERT_EQ(0, app.run(argc, argv));
}
