/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <serializers.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class serializers_test : public testing::Test
{
};


TEST_F(serializers_test, test_serialize_symbol)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("test");

  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  dump(stream, s1);
  symbol s2(table, 0, 0);
  stream.seekg(0);
  load(stream, s2);
  ASSERT_TRUE(std::equal_to<symbol>()(s1, s2));
}


TEST_F(serializers_test, test_serialize_sequence)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("unit");
  auto s2 = table->add("test");
  sequence seq1({s1, s2});

  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  dump(stream, seq1);
  sequence seq2({symbol(table, 0, 0), symbol(table, 0, 0)});
  stream.seekg(0);
  load(stream, seq2);
  ASSERT_TRUE(std::equal_to<sequence>()(seq1, seq2));
}


TEST_F(serializers_test, test_serialize_symbols_table)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  table->add("unit");
  table->add("test");

  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  dump(stream, *table);
  table->add("textgen");
  load(stream, *table);
  ASSERT_EQ(8, table->size());
  ASSERT_THROW(table->get("textgen"), not_found_error);
}


TEST_F(serializers_test, test_serialize_model)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("wolf");
  auto s2 = table->add("eats");
  auto s3 = table->add("sheep");
  auto s4 = table->add("pony");

  auto m1 = std::make_shared<model>(1, table);
  m1->add(sequence({s1}), s2, model::max_weight);
  m1->add(sequence({s2}), s3, 0);
  m1->add(sequence({s2}), s4, model::max_weight);

  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  dump(stream, *m1);
  /// reset
  auto m2 = std::make_shared<model>(0, std::make_shared<symbols_table>());
  load(stream, *m2);
  ASSERT_EQ(std::string("wolf"), m2->resolve("wolf").str());
  ASSERT_TRUE(std::equal_to<symbol>()(m2->resolve("eats"), m2->next(sequence({m2->resolve("wolf")}), 1)));
  ASSERT_TRUE(std::equal_to<symbol>()(m2->resolve("pony"), m2->next(sequence({m2->resolve("eats")}), 1)));
}

TEST_F(serializers_test, test_serialize_empty_model)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto m1 = std::make_shared<model>(1, table);
  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  dump(stream, *m1);
  /// reset
  auto m2 = std::make_shared<model>(0, std::make_shared<symbols_table>());
  load(stream, *m2);
  ASSERT_THROW(m2->resolve("test"), not_found_error);
}


TEST_F(serializers_test, test_serialize_fails_if_data_corrupted)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("wolf");
  auto s2 = table->add("eats");
  auto s3 = table->add("sheep");
  auto m1 = std::make_shared<model>(1, table);

  std::stringstream stream(std::stringstream::in | std::stringstream::out);
  m1->add(sequence({s1}), s2, model::max_weight);
  m1->add(sequence({s2}), s3, model::max_weight);
  dump(stream, *m1);
  stream.seekg(20);
  auto m2 = std::make_shared<model>(0, std::make_shared<symbols_table>());
  ASSERT_THROW(load(stream, *m2), invalid_data);
}
