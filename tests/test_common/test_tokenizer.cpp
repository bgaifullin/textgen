/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <tokenizer.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class tokenizer_test : public testing::Test
{
};


TEST_F(tokenizer_test, test_split_tokens_in_us)
{
  std::vector<std::string> result;
  tokenizer t(
    [&result](std::string&& s) mutable { result.emplace_back(std::forward<std::string>(s)); },
    [](code_point cp, const std::locale& loc) -> bool { return std::isspace(static_cast<wchar_t>(cp), loc); });

  const char text[] = " WoLf eAts SHEE";
  t(text, (sizeof(text) / sizeof(text[0])) - 1);
  t("P", 1);
  t(0, 0);

  ASSERT_EQ(3, result.size());
  ASSERT_EQ("wolf", result[0]);
  ASSERT_EQ("eats", result[1]);
  ASSERT_EQ("sheep", result[2]);
}

TEST_F(tokenizer_test, test_split_tokens_in_ru)
{
  std::vector<std::string> result;
  tokenizer t(
    [&result](std::string&& s) mutable { result.emplace_back(std::forward<std::string>(s)); },
    [](code_point cp, const std::locale& loc) -> bool { return std::isspace(static_cast<wchar_t>(cp), loc); });

  const char part1[] = u8" Да здравствует Ми";
  const char part2[] = u8"Р";

  t(part1, (sizeof(part1) / sizeof(part1[0])) - 1);
  t(part2, (sizeof(part2) / sizeof(part2[0])) - 1);
  t(0, 0);

  ASSERT_EQ(3, result.size());
  ASSERT_EQ("да", result[0]);
  ASSERT_EQ("здравствует", result[1]);
  ASSERT_EQ("мир", result[2]);
}
