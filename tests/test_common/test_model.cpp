/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <model.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class model_test : public testing::Test
{
};


TEST_F(model_test, test_get_next)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("wolf");
  auto s2 = table->add("eats");
  auto s3 = table->add("sheep");
  auto s4 = table->add("pony");

  auto m = std::make_shared<model>(1, table);
  std::uint32_t seed = 0;
  m->add(sequence({s1}), s2, model::calculate_weight(seed, 1, 1));
  seed = 0;
  m->add(sequence({s2}), s4, model::calculate_weight(seed, 1, 4));
  m->add(sequence({s2}), s3, model::calculate_weight(seed, 3, 4));

  std::equal_to<symbol> pred;
  ASSERT_TRUE(pred(s2, m->next(sequence({s1}), 1)));
  ASSERT_TRUE(pred(s3, m->next(sequence({s2}), 500)));
  ASSERT_TRUE(pred(s3, m->next(sequence({s2}), 1000)));
  ASSERT_TRUE(pred(s4, m->next(sequence({s2}), 90)));
  ASSERT_THROW(m->next(sequence({s3}), 100), not_found_error);
}


TEST_F(model_test, test_add_fails_if_rank_mismatch)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("wolf");
  auto s2 = table->add("eats");
  auto s3 = table->add("sheep");

  auto m = std::make_shared<model>(1, table);
  ASSERT_THROW(m->add(sequence({s1, s2}), s3, 1000), invalid_data);
}
