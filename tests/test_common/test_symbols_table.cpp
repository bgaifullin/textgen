/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <symbols.hpp>

#include <gtest/gtest.h>

using namespace textgen;


class symbols_table_test : public testing::Test
{
};

TEST_F(symbols_table_test, test_deduplication)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("test");
  ASSERT_EQ(4, table->size());
  table->add("test");
  ASSERT_EQ(4, table->size());
  table->add("some text");
  table->add("another text");
  table->add("this is symbols_table_test");
  auto s = table->size();
  table->add("test");
  ASSERT_EQ(s, table->size());
  ASSERT_TRUE(std::equal_to<symbol>()(s1, table->get("test")));
  ASSERT_EQ(0, s1.offset());
}

TEST_F(symbols_table_test, test_get_fails_if_symbol_not_found)
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  ASSERT_THROW(table->get("test"), not_found_error);
  table->add("test");
  ASSERT_THROW(table->get("test1"), not_found_error);
  ASSERT_THROW(table->get("tes"), not_found_error);
  ASSERT_THROW(table->get("est"), not_found_error);
}
