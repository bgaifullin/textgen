/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <stream.hpp>
#include <serializers.hpp>
#include <trainer/train_command.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace textgen;

using testing::_;

class data_reader_mock : public data_reader
{
public:
  virtual void process(const std::vector<std::string>& urls, data_consumer consumer)
  {
    const std::string& content = get_content(urls);
    consumer(content.data(), content.size());
    consumer(0, 0);
  }

  MOCK_METHOD1(get_content, std::string (const std::vector<std::string>& urls));
};

class train_command_mock : public trainer::train_command
{
public:
  MOCK_CONST_METHOD2(make_data_reader, std::unique_ptr<data_reader> (const variables_map&, logger::ptr));
};


class train_command_test : public testing::Test
{
protected:
  train_command_mock command_;
};


TEST_F(train_command_test, test_run_command)
{
  std::unique_ptr<data_reader_mock> reader = std::make_unique<data_reader_mock>();
  ON_CALL(*reader, get_content(_)).WillByDefault(testing::Return("this is test"));
  EXPECT_CALL(*reader, get_content(std::vector<std::string>({"http://localhost/file.txt"}))).Times(1);

  ON_CALL(command_, make_data_reader(_, _)).WillByDefault(testing::Return(testing::ByMove(std::move(reader))));
  EXPECT_CALL(command_, make_data_reader(_, _)).Times(1);

  const std::vector<std::string> cmdline({"-R", "1", "-S" "http://localhost/file.txt"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();

  testing::internal::CaptureStdout();
  testing::internal::CaptureStderr();
  command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stderr()));

  model::ptr m = std::make_shared<model>(1, std::make_shared<symbols_table>());
  std::istringstream stream(testing::internal::GetCapturedStdout());
  load(stream, *m);
  testing::internal::GetCapturedStderr();  // reset stderr

  ASSERT_EQ(1, m->rank());
  ASSERT_EQ("test", m->resolve("test").str());
  ASSERT_EQ("is", m->next(m->resolve_all(std::vector<std::string>({"this"})), model::max_weight).str());
}

TEST_F(train_command_test, test_fail_if_rank_is_zero)
{
  const std::vector<std::string> cmdline({"-R", "0", "-S" "http://localhost/file.txt"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();

  ASSERT_THROW(command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stderr())), invalid_data);
}
