/**
Created by Bulat Gaifullin on 20/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <stream.hpp>
#include <consumer/consume_command.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace textgen;

class consumer_command_mock : public consumer::consume_command
{
public:
  MOCK_CONST_METHOD1(load_model, model::const_ptr (const std::string&));
};


class consumer_command_test : public testing::Test
{
public:
  void SetUp();
protected:
  consumer_command_mock command_;
};


void consumer_command_test::SetUp()
{
  symbols_table::ptr table = std::make_shared<symbols_table>();
  auto s1 = table->add("wolf");
  auto s2 = table->add("eats");
  auto s3 = table->add("sheep");
  model::ptr m = std::make_shared<model>(1, table);
  m->add(sequence({s1}), s2, model::max_weight);
  m->add(sequence({s2}), s3, model::max_weight);
  ON_CALL(command_, load_model(std::string("model.bin"))).WillByDefault(testing::Return(m));
  EXPECT_CALL(command_, load_model(std::string("model.bin"))).Times(1);
}

TEST_F(consumer_command_test, test_run_command)
{
  const std::vector<std::string> cmdline({"-M", "model.bin", "-L", "2", "-I", "wolf"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();
  testing::internal::CaptureStdout();
  command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stdout()));
  ASSERT_EQ("wolf eats sheep \n", testing::internal::GetCapturedStdout());
}

TEST_F(consumer_command_test, test_consume_interupts_if_there_is_no_more_symbols)
{
  const std::vector<std::string> cmdline({"-M", "model.bin", "-L", "10", "-I", "wolf"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();
  testing::internal::CaptureStdout();
  command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stdout()));
  ASSERT_EQ("wolf eats sheep \n", testing::internal::GetCapturedStdout());
}

TEST_F(consumer_command_test, test_error_if_initial_words_count_does_not_equal_rank)
{
  const std::vector<std::string> cmdline({"-M", "model.bin", "-L", "1", "-I", "wolf", "-I", "eats"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();
  ASSERT_THROW(command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stdout())), invalid_data);
}

TEST_F(consumer_command_test, test_error_if_words_from_initial_sequence_does_not_found)
{
  const std::vector<std::string> cmdline({"-M", "model.bin", "-L", "1", "-I", "test"});
  auto options = command_.get_options();
  boost::program_options::variables_map vars;
  boost::program_options::store(boost::program_options::command_line_parser(cmdline).options(options).run(), vars);
  vars.notify();
  ASSERT_THROW(command_.take_action(vars, std::make_shared<logger>(logger::ERROR, open_stdout())), invalid_data);
}
