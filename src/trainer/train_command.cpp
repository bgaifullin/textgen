/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <data_reader.hpp>
#include <tokenizer.hpp>
#include <error.hpp>
#include <serializers.hpp>
#include <trainer/builder.hpp>
#include <trainer/train_command.hpp>

namespace textgen {
namespace {

struct is_separator : std::binary_function<bool, code_point, std::locale>
{
  bool operator()(code_point c, const std::locale& loc) const
  {
    if (c == '-' || c == '`')
      return false;
    return (std::isspace(static_cast<wchar_t >(c), loc) || std::ispunct(static_cast<wchar_t >(c), loc));
  }
};

} // anonymous

namespace trainer {

options_description train_command::get_options() const
{
  options_description desc("train command options");
  desc.add_options()
    (
      "rank,R",
      boost::program_options::value<std::size_t>()->required(),
      "The rank of model, a positive number between 1 and 255"
    )
    (
      "source,S",
      boost::program_options::value<std::vector<std::string>>()->multitoken()->required(),
      "One or more URLs with source files"
    )
    (
      "output,O",
      boost::program_options::value<std::string>()->default_value(std::string()),
      "The file path for saving model. by default standard output will be used"
    )
    (
      "retry-num",
      boost::program_options::value<std::uint16_t>()->default_value(3),
      "The number of attempts to download source in case if it is not available"
    )
    (
      "retry-delay",
      boost::program_options::value<std::uint16_t>()->default_value(0),
      "The delay in seconds between retries"
    );

  return desc;
}

void train_command::take_action(const variables_map& vars, logger::ptr log)
{
  const std::string& output_path = vars["output"].as<std::string>();
  // open file at the beginning to detect possible troubles early
  // error on file open should be handled on caller site
  ostream_holder output(output_path.empty() ? open_stdout() : open_for_write(output_path));
  const std::size_t rank = vars["rank"].as<std::size_t>();
  const std::vector<std::string> sources(vars["source"].as<std::vector<std::string>>());
  if (!rank)
  {
    throw invalid_data("rank should be greater than 0");
  }
  builder model_builder(rank);
  std::unique_ptr<data_reader> reader = make_data_reader(vars, log);

  reader->process(
    sources,
    tokenizer(
      [&model_builder] (std::string&& s) mutable { model_builder.add(std::forward<std::string>(s)); },
      is_separator()));

  auto m = model_builder.get_result();
  dump(*output, *m);
}

std::unique_ptr<data_reader> train_command::make_data_reader(const variables_map& vars, logger::ptr log) const
{
  return std::make_unique<default_data_reader>(
    log, vars["retry-num"].as<std::uint16_t>(), vars["retry-delay"].as<std::uint16_t>());
}

} // trainer
} // textgen
