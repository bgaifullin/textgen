/**
Created by Bulat Gaifullin on 18/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <trainer/builder.hpp>

#include <algorithm>

namespace textgen {
namespace trainer {

builder::builder(std::size_t model_rank)
  : model_rank_(model_rank)
  , symbols_(std::make_shared<symbols_table>())
{
  consumer_ = &builder::init_sequence;
}

void builder::add(std::string&& s)
{
  (this->*consumer_)(symbols_->add(std::forward<std::string>(s)));
}

void builder::init_sequence(symbol&& s)
{
  initial_vec_.push_back(s);
  if (initial_vec_.size() >= model_rank_)
  {
    current_sequence_ = std::make_unique<sequence>(std::move(initial_vec_));
    consumer_ = &builder::add_to_model;
  }
}

void builder::add_to_model(symbol&& s)
{
  cache_[*current_sequence_][s] += 1;
  current_sequence_->shift_and_eat(std::forward<symbol>(s));
}

model::ptr builder::get_result() const
{
  model::ptr result = std::make_shared<model>(model_rank_, symbols_);

  for (const auto& i : cache_)
  {
    std::multimap<std::uint32_t, symbol> tmp;
    std::uint32_t total = 0;
    for (const auto& j : i.second)
    {
      tmp.insert(std::make_pair(j.second, j.first));
      total += j.second;
    }
    // it is important to insert with keep order
    std::uint32_t seed = 0;
    for (const auto& k : tmp)
    {
      result->add(i.first, k.second, model::calculate_weight(seed, k.first, total));
    }
  }
  return result;
}

} // trainer
} // textgen
