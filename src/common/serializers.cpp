/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <serializers.hpp>


namespace textgen {

namespace {

inline void check_stream(std::istream& s)
{
  if (s.fail())
  {
    throw invalid_data("Cannot read from stream. Please check that file exists and is not corrupted.");
  }
}

inline void check_stream(std::ostream& s)
{
  if (s.fail())
  {
    throw io_error("Cannot write to stream. Please check that there is enough space on disk.");
  }
}

template<typename T>
inline void dump(std::ostream& s, typename std::enable_if<std::is_integral<T>::value, T>::type v)
{
  s.write(reinterpret_cast<const char*>(&v), sizeof(T));
}

// the size of std::size_t is platform depended, we will always use 64 bit int to store size.
inline void dump_s(std::ostream& s, std::size_t v)
{
  dump<std::uint64_t>(s, static_cast<std::uint64_t>(v));
}


inline void dump(std::ostream& s, const std::vector<char>& v)
{
  dump_s(s, v.size());
  s.write(v.data(), v.size());
}

template<typename T>
inline T load(std::istream& s, typename std::enable_if<std::is_integral<T>::value, T>::type v=T())
{
  s.read(reinterpret_cast<char*>(&v), sizeof(T));
  return v;
}

inline std::size_t load_s(std::istream& s)
{
  return static_cast<std::size_t>(load<std::uint64_t>(s));
}

inline void load(std::istream& s, std::vector<char>& v)
{
  std::size_t tmp = load_s(s);
  if (tmp)
  {
    v = std::move(std::vector<char>(tmp));
    s.read(v.data(), v.size());
  }
}

} //anonymous

void dump(std::ostream& s, const symbols_table& o)
{
  // first 8 byte is size, other is data
  dump(s, o.storage_);
  check_stream(s);
  s << std::endl;
  dump_s(s, o.map_.size());
  for (auto& i : o.map_)
  {
    dump_s(s, i.second);
    dump_s(s, i.first.size());
  }
  check_stream(s);
  s << std::endl;
}

void load(std::istream& s, symbols_table& o)
{
  load(s, o.storage_);
  check_stream(s);
  s.ignore();

  std::size_t map_size = load_s(s);
  check_stream(s);

  for (; map_size > 0; --map_size)
  {
    std::size_t offset = load_s(s);
    std::size_t size = load_s(s);
    check_stream(s);
    o.map_[details::string_ref(o.storage_, offset, size)] = offset;
  }
  s.ignore();
}


void dump(std::ostream& s, const symbol& o)
{
  dump_s(s, o.offset());
  dump_s(s, o.size());
  check_stream(s);
}


void load(std::istream& s, symbol& o)
{
  o.offset_ = load_s(s);
  o.size_ = load_s(s);
  check_stream(s);
}


void dump(std::ostream& s, const sequence& o)
{
  for (const auto& i: o)
  {
    dump(s, i);
  }
}

void load(std::istream& s, sequence& o)
{
  // invalidate hash_sum
  o.hash_ = 0;
  for (auto& i: o.items_)
  {
    load(s, i);
    o.on_push(i);
  }
}

void dump(std::ostream& s, const model& o)
{
  // TODO(bgaifullin) what about crc32 for header
  dump_s(s, o.rank_);
  dump_s(s, o.mark_chain_.size());
  check_stream(s);
  s << std::endl;

  dump(s, *o.symbols_);

  // chains
  for (auto const& items : o.mark_chain_)
  {
    dump_s(s, items.second.size());
    dump(s, items.first);
    s << std::endl;

    for (auto const& value : items.second )
    {
      dump<std::uint32_t>(s, value.first);
      dump(s, value.second);
      s << std::endl;
    }
  }
  check_stream(s);
}

void load(std::istream& s, model& o)
{
  o.rank_ = load_s(s);
  std::size_t chain_size = load_s(s);
  check_stream(s);
  s.ignore();
  load(s, *o.symbols_);

  sequence tmp_seq(sequence::container_type(o.rank_, symbol(o.symbols_, 0, 0)));
  symbol tmp_sym(o.symbols_, 0, 0);

  for (; chain_size > 0; --chain_size)
  {
    std::size_t choices_size = load_s(s);
    load(s, tmp_seq);
    check_stream(s);
    s.ignore();

    for (; choices_size > 0; --choices_size)
    {
      std::uint32_t weigth = load<std::uint32_t>(s);
      load(s, tmp_sym);
      check_stream(s);
      s.ignore();
      o.add(tmp_seq, tmp_sym, weigth);
    }
  }
}

}
