/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <logger.hpp>

#include <ctime>
#include <iomanip>

namespace textgen {

namespace {

/**
 * converts logger level to human readable format
 */
const char* level2text(logger::level l)
{
  switch(l)
  {
    case logger::DEBUG:
      return "DEBUG";
    case logger::INFO:
      return "INFO";
    case logger::WARNING:
      return "WARNING";
    case logger::ERROR:
      return "ERROR";
  }
}

}

void logger::write(level l, const std::string& message)
{
  std::time_t now = std::time(nullptr);
  *stream_ << level2text(l) << " : " << std::put_time(std::localtime(&now), "%c") << " : " << message << std::endl;
}

} // textgen