/**
Created by Bulat Gaifullin on 15/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <data_reader.hpp>
#include <error.hpp>

#include <boost/filesystem/operations.hpp>
#include <boost/process.hpp>

#ifdef __linux__
#include <linux/limits.h>
#else
#include <limits.h>
#endif

namespace bp = boost::process;
namespace bf = boost::filesystem;

namespace textgen {
namespace details {

const std::string get_curl_path()
{
  if(const char* env_p = std::getenv("PATH"))
  {
    const char* s = env_p;
    for (; *env_p; ++env_p)
    {
      if (*env_p == ':')
      {
        bf::path p(s, env_p);
        p /= "curl";
        if (boost::filesystem::exists(p))
        {
          return p.string();
        }
        s = ++env_p;
      }
    }
  }
  throw error("curl utility is not found in ${PATH}, Please add location of curl to ${PATH}");
}

const std::size_t BUFFER_SIZE = 4096;

void read_from_stream(std::istream& stream, std::vector<char>& read_buffer, data_consumer consumer)
{
  char* s = read_buffer.data();
  std::size_t n = read_buffer.size();
  for (;;)
  {
    stream.read(s, n);
    consumer(s, static_cast<std::size_t>(stream.gcount()));
    if (stream.eof())
    {
      // sentinel
      consumer(0, 0);
      break;
    }
    if (stream.fail())
    {
      throw reader_error("cannot read from child process output stream");
    }
  }
}

void curl_data_reader::process(const std::vector<std::string>& urls, data_consumer consumer)
{
  std::vector<std::string> args;
  args.push_back("-s");
  args.push_back("-S");
  args.push_back("-f");
  args.push_back("--retry");
  args.push_back(std::to_string(retry_num_));
  args.push_back("--retry-delay");
  args.push_back(std::to_string(retry_delay_));

  std::vector<char> read_buffer(BUFFER_SIZE);

  const std::size_t offset = args.size();
  const std::size_t args_max = ARG_MAX - 100;
  std::size_t args_size = 0;

  for (auto url: urls)
  {
    args_size += (url.size() + 3);
    if (args_size >= args_max)
    {
      logger_->debug("ARG_MAX exceeded. download next batch of urls.");
      download(args, consumer, read_buffer);
      args_size = url.size() + 3;
      args.erase(args.begin() + offset, args.end());
    }
    logger_->debug("add %s to batch.", url);
    args.push_back(url);
    args.push_back("-o");
    args.push_back("-");
  }

  if (args.size() != offset)
  {
    logger_->debug("download batch of urls.");
    download(args, consumer, read_buffer);
  }
}

void curl_data_reader::download(std::vector<std::string>& args, data_consumer& consumer, std::vector<char>& read_buffer)
{
  logger_->debug("downloading...");
  const std::string curl_bin(get_curl_path());
  bp::context ctx;
  ctx.stdout_behavior = bp::capture_stream();
  ctx.stderr_behavior = bp::capture_stream();
  auto p = bp::launch(curl_bin, args, ctx);

  read_from_stream(p.get_stdout(), read_buffer, consumer);

  auto status = p.wait();
  if (!status.exited())
  {
    logger_->debug("try to terminate process.");
    p.terminate(1);
    throw reader_error("seems like process hangs.");
  }
  if (status.exit_status())
  {
    logger_->debug("downloading failed.");
    std::string err_msg;
    auto tmp = [&err_msg](const char* s, std::size_t n) mutable -> void { err_msg.append(s, n); };
    read_from_stream(p.get_stderr(), read_buffer, tmp);
    throw reader_error(err_msg);
  }
  logger_->debug("downloading completed.");
}

} // details
} // textgen
