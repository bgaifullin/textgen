/**
Created by Bulat Gaifullin on 16/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <tokenizer.hpp>

#include <boost/locale/conversion.hpp>
#include <boost/locale/generator.hpp>

#include <cstring>

namespace bl = boost::locale;

namespace textgen {

namespace {

std::locale get_default_locale()
{
  const char* c_type = std::getenv("LC_CTYPE");
  return bl::generator()(c_type ? c_type : "en_US.UTF-8");
}

} // anonymous

typedef bl::utf::utf_traits<char, 1> utf8_traits;

tokenizer::tokenizer(token_consumer&& consumer, separator&& sep)
  : consumer_(std::forward<token_consumer>(consumer))
  , separator_(std::forward<separator>(sep))
  , tail_buffer_(0)
  , tail_size_(0)
  , locale_(get_default_locale())
{
}


void tokenizer::operator()(const char* s, std::size_t n)
{
  if (n == 0)
  {
    if (tail_size_ != 0)
    {
      throw invalid_data("some symbols was not decoded. input data does not contain valid utf-8 encoded text.");
    }
    on_next_token();
  }
  else
  {
    on_next_chunk(s, n);
  }
}

void tokenizer::on_next_chunk(const char* s, std::size_t n)
{
  std::size_t i = 0;
  std::size_t needs = sizeof(code_point) - tail_size_;
  while (i < n)
  {
    char* ptr = reinterpret_cast<char*>(&tail_buffer_);
    // get next portion
    if (needs)
    {
      if (i + needs >= n)
      {
        needs = n - i;
      }
      std::memmove(ptr + tail_size_, s + i, needs);
      tail_size_ += needs;
      i += needs;
    }

    const char* start = ptr;
    const char* cursor = start;
    const char* end = start + tail_size_;

    /// eat all valid chars
    while (start != end)
    {
      code_point cp = utf8_traits::decode(cursor, end);
      if (cp == bl::utf::incomplete)
      {
        break;
      }
      if (cp == bl::utf::illegal)
      {
        throw invalid_data("cannot decode symbol. input data does not contain valid utf-8 encoded text.");
      }

      if (separator_(cp, locale_))
      {
        on_next_token();
      }
      else
      {
        buffer_.insert(buffer_.end(), start, cursor);
      }
      start = cursor;
    }
    needs = start - ptr;
    tail_size_ -= needs;
    /// shift buffer (number of bytes * bits per byte)
    tail_buffer_ >>= (needs * 8);
  }
}

void tokenizer::on_next_token()
{
  if (!buffer_.empty())
  {
    const char* s = buffer_.data();
    try
    {
      consumer_(bl::to_lower(s, s + buffer_.size(), locale_));
    }
    catch (const std::bad_cast&)
    {
      throw error("failed to convert string to lowercase. "
                    "Please ensure that ${LC_TYPE} points to locale which supports UTF-8.");
    }
    buffer_.clear();
  }
}

}
