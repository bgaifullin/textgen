/**
Created by Bulat Gaifullin on 15/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/


#include <stream.hpp>

#include <fstream>


namespace textgen {

namespace {

template<typename T>
struct null_deleter : std::unary_function<void, T*>
{
  void operator()(T*) const
  {
  }
};

template<typename T>
struct get_openmode {};

template <>
struct get_openmode<std::ifstream>
{
  enum
  {
    value = std::istream::in
  };
};

template <>
struct get_openmode<std::ofstream>
{
  enum
  {
    value = std::ostream::out | std::ostream::trunc
  };
};


template <typename T>
std::unique_ptr<T> open_stream(const std::string& filepath)
{
  return std::make_unique<T>(filepath, static_cast<typename T::openmode>(get_openmode<T>::value));
};

} //anonymous

istream_holder open_for_read(const std::string& filepath)
{
  return istream_holder(open_stream<std::ifstream>(filepath).release(), std::default_delete<std::istream>());
}

ostream_holder open_for_write(const std::string& filepath)
{
  return ostream_holder(open_stream<std::ofstream>(filepath).release(), std::default_delete<std::ostream>());
}

istream_holder open_stdin()
{
  return istream_holder(&std::cin, null_deleter<std::istream>());
}

ostream_holder open_stdout()
{
  return ostream_holder(&std::cout, null_deleter<std::ostream>());
}

ostream_holder open_stderr()
{
  return ostream_holder(&std::cerr, null_deleter<std::ostream>());
}

} // textgen
