/**
Created by Bulat Gaifullin on 12/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <application.hpp>
#include <error.hpp>
#include <logger.hpp>

#include <algorithm>
#include <iostream>


namespace po = boost::program_options;

namespace textgen {

namespace {


logger::level get_logger_level(const po::variables_map& vars)
{
  if (vars["debug"].as<bool>())
    return logger::DEBUG;
  if (vars["verbose"].as<bool>())
    return logger::INFO;
  return logger::WARNING;
}

} // anonymous


int application::run(int argc, const char** argv) noexcept
{
  try
  {
    start(argc, argv);
    return 0;
  }
  catch (const po::error& e)
  {
    std::cerr << "Error: " << e.what() << std::endl;
    std::cerr << "Please use --help to get additional information about usage." << std::endl;
  }
  catch (const error& e)
  {
    std::cerr << e.what() << std::endl;
  }
  catch (const std::exception& e)
  {
    std::cerr << "Unexpected error: " << e.what() << std::endl;
  }
  catch (...)
  {
    std::cerr << "Uncaught exception!" << std::endl;
  }
  return 1;
}


void application::start(int argc, const char** argv)
{
  po::options_description all("usage: textgen [common options] --{command} [command options]");
  all.add(get_common_options()).add(get_available_commands());

  auto common_args(po::command_line_parser(argc, argv).options(all).allow_unregistered().run());
  po::variables_map vars;
  po::store(common_args, vars);

  command::ptr cmd = find_command(vars);

  if (vars.count("help"))
  {
    std::cout << (cmd ? cmd->get_options() : all) << std::endl;
    return;
  }

  if (!cmd)
  {
    throw po::required_option("{command}");
  }

  auto cmd_options(cmd->get_options());
  auto unrecognized_args(po::collect_unrecognized(common_args.options, po::include_positional));
  po::store(po::command_line_parser(unrecognized_args).options(cmd_options).run(), vars);

  vars.notify();
  run_command(cmd, vars);
}

void application::run_command(command::ptr cmd, const variables_map& vars)
{
  auto log = std::make_shared<logger>(get_logger_level(vars), open_stderr());
  cmd->take_action(vars, log);
}

options_description application::get_common_options() const
{
  po::options_description desc("common options");
  desc.add_options()
    ("help", "show help")
    ("debug,D", po::bool_switch()->default_value(false), "enable debug output")
    ("verbose,V", po::bool_switch()->default_value(false), "enable verbose output");
  return desc;
}

options_description application::get_available_commands() const
{
  po::options_description available_commands("available commands");
  for (auto command: commands_)
  {
    available_commands.add_options()(command->name(), command->description());
  }

  return available_commands;
}

command::ptr application::find_command(const variables_map& parsed) const
{
  // we can use linear search because number of commands is small
  // we should check all commands to find duplicates
  command::ptr result;

  for (auto command: commands_)
  {
    if (parsed.count(command->name()))
    {
      if (result)
      {
        po::multiple_occurrences exc;
        exc.add_context("{command}", command->name(), 0);
        throw exc;
      }
      result = command;
    }
  }

  return result;
}

} // textgen
