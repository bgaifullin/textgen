/**
Created by Bulat Gaifullin on 18/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <symbols.hpp>


namespace textgen {

symbol symbols_table::add(const std::string& s)
{
  auto pos = map_.find(details::string_ref(s));
  if (pos != map_.end())
  {
    return symbol(shared_from_this(), pos->second, s.size());
  }
  auto offset = storage_.size();
  storage_.insert(storage_.end(), s.begin(), s.end());
  map_[details::string_ref(storage_, offset, s.size())] = offset;
  return symbol(shared_from_this(), offset, s.size());
}


symbol symbols_table::get(const std::string& s) const
{
  auto pos = map_.find(details::string_ref(s));
  if (pos == map_.end())
  {
    throw not_found_error(s);
  }
  return symbol(shared_from_this(), pos->second, s.size());
}

} // textgen
