/**
Created by Bulat Gaifullin on 18/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <model.hpp>

#include <random>

namespace textgen {

void model::add(const sequence& seq, const symbol& sym, std::uint32_t weight)
{
  if (seq.size() != rank_)
  {
    throw invalid_data("Length of sequence shoud be equal rank of model");
  }
  mark_chain_[seq].insert(std::make_pair(weight, sym));
}

symbol model::next(const sequence& seq, std::uint32_t weight) const
{
  auto it = mark_chain_.find(seq);
  if (it == mark_chain_.end())
  {
    throw not_found_error("sequence is not found.");
  }
  auto& mapping = it->second;
  auto it2 = mapping.lower_bound(weight);
  for (; it2 != mapping.end(); ++it2)
  {
    if (it2->first >= weight)
    {
      return it2->second;
    }
  }
  throw invalid_data("invalid wheight.");
}

} // textgen
