/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <consumer/generator.hpp>


namespace textgen {
namespace consumer {

text_generator::text_generator(model::const_ptr m, const std::vector<std::string>& initial_sec, std::size_t l)
  : model_(m)
  , sequence_(model_->resolve_all(initial_sec))
  , length_(l)
  , random_engine_(std::random_device()())
{
}

std::string text_generator::next()
{
  if (length_ > 0)
  {
    --length_;
    return get_next();
  }
  return sequence_.shift().str();
}

std::string text_generator::get_next()
{
  std::uniform_int_distribution<std::uint32_t> uniform_dist(0, model::max_weight);
  try
  {
    return sequence_.shift_and_eat(model_->next(sequence_, uniform_dist(random_engine_))).str();
  }
  catch (const not_found_error&)
  {
    length_ = 0;
  }
  return sequence_.shift().str();
}

bool text_generator::is_valid() const
{
  return length_ || sequence_.size();
}

} // consumer
} // textgen
