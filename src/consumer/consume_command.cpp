/**
Created by Bulat Gaifullin on 19/09/16.

This file is part of textgen
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**/

#include <error.hpp>
#include <model.hpp>
#include <serializers.hpp>
#include <consumer/consume_command.hpp>
#include <consumer/generator.hpp>

namespace textgen {
namespace consumer {

options_description consume_command::get_options() const
{
  options_description desc("consume command options");
  desc.add_options()
    (
      "initial,I",
      boost::program_options::value<std::vector<std::string>>()->multitoken()->required(),
      "The initial phrase (space separated words) which should has length equal to rank of model. All in lower case!"
    )
    (
      "length,L",
      boost::program_options::value<std::uint32_t>()->required(),
      "How many words should be consumed."
    )
    (
      "model,M",
      boost::program_options::value<std::string>()->default_value(std::string()),
      "The path to file which contains model, by default it reads from standard input"
    );
  return desc;
}

void consume_command::take_action(const variables_map& vars, logger::ptr log)
{
  const std::string& db_path = vars["model"].as<std::string>();
  std::uint32_t length = vars["length"].as<std::uint32_t>();
  const std::vector<std::string> initial_vec(vars["initial"].as<std::vector<std::string>>());

  if (!length)
  {
    throw invalid_data("the length should be greater than 0");
  }

  model::const_ptr m = load_model(db_path);
  if (initial_vec.size() != m->rank())
  {
    throw invalid_data("The initial words count should be equal rank of model.");
  }

  auto generator_ptr = make_generator(m, initial_vec, length);
  auto& generator = *generator_ptr;

  for (; generator.is_valid();)
  {
    std::cout << generator.next() << ' ';
  }
  std::cout << std::endl;
}

model::const_ptr consume_command::load_model(const std::string& filepath) const
{
  istream_holder db_stream(filepath.empty() ? open_stdin() : open_for_read(filepath));
  model::ptr m(std::make_shared<model>(0, std::make_shared<symbols_table>()));
  load(*db_stream, *m);
  return m;
}

std::unique_ptr<text_generator> consume_command::make_generator(
  model::const_ptr m, const std::vector<std::string>& v, std::size_t l) const
{
  try
  {
    return std::make_unique<text_generator>(m, v, l);
  }
  catch (const not_found_error& e)
  {
    std::string message = "The following item is not found in model: ";
    message.append(e.what()).append(". Please ensure that syntax correct and tokens write in lower case.");
    throw invalid_data(message);
  }
}

} // trainer
} // textgen
