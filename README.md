The text generator based on [Markov chain][1]
=============================================
 
Requirements
============
- cmake (version >= 2.8)
- make
- C++ compiler with supports c++14 standard (clang >= 3.9 || gcc >= 6.2.0)
- [boost][2]: (>= 1.54)
    - boost-filesystem
    - boost-program-options
    - boost-locale
- [boost/process][3] (it is not official part of boost, so should be downloaded and installed separately)

Build
=====
```
[[ -d build ]] || mkdir build
cd build && cmake ../ && make
cd ..
```

Run test
========
```
cd build
cmake test ../ && make test
```

Usage
=====

to view list of available commands please use
```
textgen --help
```

to view list of options for command please use
```
textgen --help --<command name>
```

[1]: https://en.wikipedia.org/wiki/Markov_chain
[2]: http://sourceforge.net/projects/boost/files/boost/1.60.0/
[3]: http://www.highscore.de/boost/process/
